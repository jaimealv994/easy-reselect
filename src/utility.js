import resolver from 'object-resolve-path';

export const getProp = propName => data => resolver(data, propName);

export const getProps = (...props) => {
  return props.map(prop => getProp(prop));
};

export const processCacheName = (...props) => data => {
  return props
    .map(key => {
      const prop = getProp(key)(data);
      return typeof prop === 'object' ? JSON.stringify(prop) : prop;
    })
    .join(':');
};
